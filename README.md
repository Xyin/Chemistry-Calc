## Synopsis
Chemistry-Calc is a program that allows you to perform several complicated proceedures involving chemicals automatically.
At the moment, Chemistry Calc only supports viewing properties of elements and balancing chemical equations.

## Installation
Download the repository and extract it to wherever you want it, and run `make`. On Linux, mark the Calculator program as executable and run it with `./Calculator`. 
On Windows, Calculator should be able to run in the terminal, though further testing is needed.

## Running
Chemistry-Calc runs only through an interactive, console environment, with no command line arguments. I plan on implementing a CLI soon, however.
After starting Calculator, simply supply the desired arguments as it asks for them. Balancing chemical equations in particular is finicky, read the built in instructions on how to input equations
properly. 

## Contributors
All contributions are done right here in GitLab. Feel free to fork the project and help out where you can! Anything you think can be helpful is no doubt appreciated.

