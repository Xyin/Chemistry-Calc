#include "main.h"
#include <stdio.h>
#include <math.h>
void flush(){
	char c;
	while((c = getchar()) != '\n' && c != '\0')
		/* discard */ ;
}


int getPos(int rows, int columns, int i, int j){
	return i*columns+j;
}

int gcd(int K, int M) {
      int k = max(K,M);
      int m = min(K,M);
      // loop invariant: k ≥ m ∧ GCD(K,M) = GCD(k,m)
      while (m != 0) {
         int r = k % m;
         k = m;
         m = r;
      }
      // At this point, GCD(K,M) = GCD(k,m) = GCD(k,0) = k
      return k;
}

int lcd(int k, int m){
	int result;
	if(!(k==0&&m==0)) result=(abs(k*m)/gcd(k,m));
	return result;
}	

int max(int num1, int num2){
	if(num1>num2) return num1;
	if(num2>=num1) return num2;
}

int min(int num1, int num2){
	if(num1<num2) return num1;
	if(num2<=num1) return num2;
}

