#include <stdio.h>
#include "main.h"
#include <stdlib.h>
#include <string.h>

int main()
{
	struct periodic *tablePtr;
	tablePtr = createTable();
	printf("INFORMATION SHUTFF HERE\n");
	printf("(1)-View information about an element.\n");
	printf("(2)-Balance a chemical equation.\n");
	printf("(0)-Exit.\n");
	int num;
	printf("Enter your answer:");
	scanf("%d",&num); //Whitespace is removed automagically
	if(num==1){ //View Information about an element.
		printf("Please enter the element number, symbol, or name:");
		char input[20]="";
		scanf("%20s",input);
		printf("%s",input);	
		printf("\n");
		int ret = defInput(input);
		int index;
		if(ret==0){
			index=atoi(input)-1;
		}else if(ret==1){
			index=searchTable(0,input,tablePtr); //The mode from defInput() turns into a zero here. It does NOT stay a one.
		}
		else if(ret==2){
			index=searchTable(1,input,tablePtr);
		}
		if(index==-1)printf("Invalid element number, symbol, or name.");
		else displayData(tablePtr+index);

	}
	if(num==2){//Balance a chemical equation.
		//Documentation
		printf("Please enter the equation to be parsed.\n");
		printf("Be sure the first letter of every element is capitalized.\n");
		printf("Do not use spaces, and use an equals sign (=) in place of an arrow\n");
		printf("This program also does not yet support parentheses, as in (HO)2. Instead, expand and type H2O2.\n");

		//Get the string from the user and reallocate memory block accordingly.
		char *equation=malloc(500);
		flush();
		fgets(equation,500,stdin);
		equation=(char*)realloc(equation,strlen(equation)*sizeof(char)+1);

		int rows,columns=1,index=0;
		char curChar;

		//Count number of columns
		do{
			curChar=equation[index];
			if(curChar=='+'||curChar=='=')columns++;
			index++;
		}
		while(curChar!='\0');

		char *tempStr;
		tempStr = malloc(strlen(equation)*sizeof(char)+1);
		strcpy(tempStr,equation);

		rows = countElements(tempStr);
		strcpy(tempStr,equation); //Ironically, the tempStr has become the mainStr.	
		int *mtxPtr = parseEq(equation,rows,columns);
		frac *newMat = calloc(rows*columns,sizeof(frac));
		convTable(mtxPtr,newMat,rows,columns);	
		solveMat(newMat,rows,columns);

		int len = rows+1;	
		frac *balance = malloc(sizeof(frac)*(len));
		retMatVals(newMat,balance,rows,columns);
		(balance+(len-1))->num=1;
		(balance+(len-1))->den=1;

		int factor = massLcd(balance,len);
		frac fact;
		fact.num=factor;
		fact.den=1;
		mulR(balance,len,0,fact);
		int i;
		for(i=0;i<len;i++)reduce(balance+i);
		dispEqu(tempStr,balance); //And this is why.
		free(mtxPtr);
		free(equation);
		free(tempStr);
		free(balance);
		free(newMat);
	}
	free(tablePtr);

	return 0;
}


