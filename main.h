#ifndef PERIODIC_H_INCLUDED
#define PERIODIC_H_INCLUDED
/*Contents of main.h:
 *1)Inclusions
 *2)Constant definitions
 *3)Type definitions
 *4)Function prototypes
 */

//Inclusions
#include <stdio.h>

//Constant Defintions
#define num_elements 118

//Type Definitions
struct periodic  //Create a structure designed to create a model of the periodic table
{
    int num;
    char sym[3]; //Symbol of Element
    char name[20]; //Name of Element
    float weight; //Atomic Mass
    char config[100]; //Configuration
    float neg; //Electronegativity
    int rad; //Radius
    float ion_rad; //Ion Radius
    int vdW_rad; //Van der Waals radius
    int IE_1; //IE-1
    int EA; //EA
    char oxi_st[40];//Oxidation states
    char stn_st[7];//Standard state
    char bond_type[17];//Bonding type
    int melt; //Melting point
    int boil; //Boiling point
    float dens; //Density
    char type[40]; //Metal or nonmetal
    char year[7]; //Year Discovered

};

typedef struct{ 
	int num; //Numerator
	int den; //Denominator
} frac;
//Buncha Declarations. Hold onto your butts.

/*Name: createTable()
 *Location: createTable.c 
 *Inputs: None
 *Outputs: A pointer to the first struct in an array of structures. Each structure is an element and its properties.
 
 *Description:Parses the file periodictable.csv in order to get all the properties of each element. Runs on program launch
 *and hopefully should not need to be called again. Format is as followed:
 *Number, Symbol
 *Name, Weight
 *Electron Config., Negativity
 *Radius, Ion Radius
 *van der Waals Radius, IE_1
 *EA, Oxidation States
 *Standard State, Bond Type
 *Melting Point, Boiling Point
 *Density, Type
 *Year
 ***TRY TO KEEP ALL REFRENCES TO THESE VALUES IN THESE TWO-LINE GROUPS.***
 */ 
struct periodic * createTable();


/*Name: displayData()
 *Location: displayData.c
 *Description: Prints all the information regarding an element to the screen. It's worth nothing that in this specific case,
 *the rule about two per line is ignored simply due to the length of the lines, and readability. 
 *Inputs: struct periodic *element -- Pointer to the structure for any given element 
 *Outputs: None
 */
void displayData(struct periodic *element);


/*Name: searchTable()
 *Location: searchTable.c 
 *Description: Searches the array of structures for a matching element based on number, symbol or name.
 *Inputs: int mode -- Search by number, symbol, or name
 *        char input[20] -- Input string
 *        struct periodic *table -- Pointer to the start of periodic table array
 * 
 *Outputs: Index of correct element
 */
int searchTable(int mode, char input[20], struct periodic *table);

/*Name: countElements()
 *Location: parseEq.c
 *Description: Counts the number of elements in an equation before any parsing actually takes place. The function actually looks a LOT like parseEq, because it works the exact same way.
 *Inputs: char *tempInput -- The equation to be counted
 *        char *elemOrder -- A pointer to the order in which elemments are listed.
 *Outputs: The number of elements in the equation
 */
int countElements(char *tempInput);

/*Name: parseEq()
 *Location: parseEq.c
 *Description: Parses a chemical equation and creates a matrix for further calculating/solving.
 *Inputs: char *input -- The equation to be parsed
 *        int rows -- The number of rows
 *        int columns -- The number of columns
 *Outputs: A pointer to the matrix 
 */
int * parseEq(char *input, int rows, int columns);

/*Name: flush()
 *Location: main.c
 *Description: Flushes STDIN
 *Inputs: None
 *Outputs: None
 */
void flush();

/*Name: displayMatrix()
 *Location: display.c
 *Description: Displays a matrix
 *Inputs: int *matPtr -- Pointer to the start of the array
 *        int rows -- Number of rows in the matrix
 *        int columns -- Number of columns in the matrix
 *        char *elemOrder -- A char array that associates each row with an element
 *Outputs: None
 */ 
void displayMatrix(int *matPtr,int rows, int columns);

/*Name: displayFMatrix()
 *Location: display.c
 *Description: Displays a FLOAT matrix
 *Inputs: float *matPtr -- Pointer to the start of the FLOAT array
 *        int rows -- Number of rows in the matrix
 *        int columns -- Number of columns in the matrix
 *        char *elemOrder -- A char array that associates each row with an element
 *Outputs: None
 */ 
void displayFMatrix(frac *matPtr, int rows, int columns);

/*Name: swapR()
 *Location: rowOp.c
 *Description: Swaps two rows in a matrix
 *Inputs: float *matPtr -- Pointer to the start of the FLOAt array
 *        int cols -- Number of columns
 *        int src -- Index of source row
 *        int dest -- Index of destination row
 *        char *elemOrder -- Char array that associates each row with an element
 *Outputs: None
 */ 
void swapR(frac *matPtr, int cols, int src, int dest);

/*Name: mulR()
 *Location: rowOp.c
 *Description: Multiplies a row by a constant. 
 *Inputs: float *matPtr -- You've probably heard this one before.
 *        int cols -- Number of columns
 *        int src -- Source row
 *        int factor -- Number to multiply by
 *Outputs: None
 */ 
void mulR(frac *matPtr, int cols, int src, frac factor);

/*Name: addR()
 *Location: rowOp.c
 *Description: Adds a row to another row, optionally multiplying by a factor
 *Inputs: float *matPtr -- I have no idea /s
 *        int cols -- Number of coumns
 *        int src -- Source Row
 *        int desc -- Destination Row
 *        int factor -- Number to multiply the source row by.
 *Outputs: None
 */
void addR(frac *matPtr, int cols, int src, int dest, frac factor);

/*Name: addFrac()
 *Location: fracOp.c
 *Description: Adds two fractions together
 *Inputs: frac op1 -- First fraction
 *	  frac op2 -- Second fraction
 *Outputs: The resulting fraction
 */
frac addFrac(frac op1, frac op2);

/*Name: mulFrac()
 *Location: fracOp.c
 *Description: Multiplies two fractions together.
 *Inputs: frac op1 -- First fraction
 *	  frac op2 -- Second fraction.
 *Outputs: The resulting fraction.
 */
frac mulFrac(frac op1, frac op2);

/*Name: getPos()
 *Location: handydandy.c
 *Description: Turns a (x,y) coordinate into a single index. Essentially matrix -> array
 *Inputs: int rows -- Number of rows
 *        int columns -- Number of columns
 *        int i -- x coordinate
 *        int j -- y coordinates
 *Outputs: Array index
 */
int getPos(int rows, int columns, int i, int j);

/*Name: gcd()
 *Location: handydandy.c
 *Description: Finds the greatest common divisor
 *Inputs: int k -- First number
 *	  int m -- Second number
 *Outputs: GCD
 */
int gcd(int k, int m);

/*Name: lcd()
 *Location: handydandy.c
 *Description: Finds the least common denominator in two numbers.
 *Inputs: int k -- First number
 *	  int m -- Second number
 *Outputs: LCD
 */
int lcd(int k, int m);

/*Name:inv()
 *Location:fracOp.c
 *Description: Inverts a fraction
 *Inputs: op1 -- Fraction
 *Outputs: Inverted fraction
 */
frac inv(frac op1);

/*Name:reduce()
 *Location:fracOp.c
 *Description: Reduces a fraction
 *Inputs: * fraction -- the location of the fraction to be reduced
 *Outputs: None
 */
void reduce(frac *fraction);

/*Name:max()/min()
 *Location:handydandy.c
 *Description:Finds the max (and min) of two values
 *Inputs: int num1 -- First number
 *	  int num2 -- Second number
 *Outputs: int -- Biggest number
 */ 
int max(int num1, int num2);
int min(int num1, int num2);

/*Name:reduce()
 *Location:fracOp.c
 *Description:Reduces a fraction
 *Inputs: frac *fraction -- Fraction to reduce
 *Outputs: None
 */
void reduce(frac *fraction);

/*Name:massLcd()
 *Location:fracOp.c
 *Description:Finds LCD of an array of fractions
 *Inputs: frac *arr -- Pointer to an array of fractions
 *	  int len -- Length of array
 *Outputs: int -- LCD of array
 */
int massLcd(frac *arr, int len);

/*Name:inv()
 *Location:fracOp.c
 *Description: Inverts a fraction
 *Inputs: frac op1 -- Fraction to invert
 *Outputs: frac -- Inverted fraction
 */
frac inv(frac op1);


#endif // PERIODIC_H_INCLUDED
