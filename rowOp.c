/*Name: rowOp.c
 *Contains: swapR(), mulR(), addR()
 *Description: Some generic row operation functions for the float matrix that will be solved.
 */

#include "main.h"
#include <stdlib.h>

void swapR(frac *matPtr,int cols, int src, int dest){
	int *tempRow = (int)calloc(cols,sizeof(int)); //Make a temp row to store numbers in
	int i,count;
	char tempChar;
	for(i=0;i<cols;i++){
		tempRow[i]=(matPtr+(dest*cols+i))->num;
		(matPtr+(dest*cols+i))->num=(matPtr+(src*cols+i))->num;
		(matPtr+(src*cols+i))->num=tempRow[i];
	}
	
	for(i=0;i<cols;i++){
		tempRow[i]=(matPtr+(dest*cols+i))->den;
		(matPtr+(dest*cols+i))->den=(matPtr+(src*cols+i))->den;
		(matPtr+(src*cols+i))->den=tempRow[i];
	}	
	
	free(tempRow);
}

void mulR(frac *matPtr, int cols, int src, frac factor){
	int i;
	for(i=0;i<cols;i++){
		(matPtr+(src*cols+i))->num=(matPtr+(src*cols+i))->num*factor.num; 
		(matPtr+(src*cols+i))->den=(matPtr+(src*cols+i))->den*factor.den; 

	}
}

void addR(frac *matPtr, int cols, int src, int dest, frac factor){
	int i;	
	for(i=0;i<cols;i++){
		*(matPtr+(dest*cols+i))=addFrac(mulFrac(*(matPtr+(src*cols+i)),factor),*(matPtr+(dest*cols+i)));			
	}
}


